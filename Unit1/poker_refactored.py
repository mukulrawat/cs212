# Refactored Poker program
import random # this is used for shuffling

# create a deck of cards
mydeck = [r+s for r in '23456789TJQKA' for s in 'SHDC']

def deal(numhands, n = 5, deck = mydeck):
	"Function to randomly deal a hand from a deck of cards"
	return [[random.choice(mydeck) for j in xrange(n)] for i in xrange(numhands)]

def poker(hands):
    "Return a list of winning hands: poker([hand, ...]) => [hand,.....]"
    return allmax(hands, key = lambda x: hand_rank(x))

def allmax(iterable, key = None):
	"Returns a list of all items equal to the max of iterable."
	m = max(iterable, key = key)
	return [element for element in iterable if key(element) == key(m)] 

def hand_rank(hand):
    "Return a value indicating the rank of a hand"
    # counts is the count of each rank; ranks lists corresponding ranks
    # E.g. '7 T 7 9 7' => counts = (3, 1, 1); ranks = (7, 10, 9)
    groups = group(['--23456789TJQKA'.index(r) for r , s in hand])
    counts,  ranks = unzip(groups)
    
    # to handle Ace Low straight
    if ranks == (14,  5,  4,  3,  2,):
        ranks = (5,  4,  3,  2,  1)
    
    # to handle straight
    straight = len(ranks) == 5 and max(ranks) - min(ranks) == 4
    
    # to handle flush
    flush = len(set([s for r, s in hands])) == 1
    
    return (9 if (5, ) == counts else
                  8 if  straight and flush else
                  7 if (4, 1) == counts else
                  6 if (3, 2) == counts else
                  5 if flush else
                  4 if straight else
                  3 if (3,  1,  1) == counts else
                  2 if (2,  2,  1) == counts else
                  1 if (2, 1,  1,  1) == counts else
                  0),  ranks
            

def group():
    "Return a list of [(count, x)...], highest count first, then highest x first."
    groups = [(items.count(x),  x) for x in set(items)]
    return sorted(groups,  reverse = True)
    
def unzip(pairs):
    return zip(*pairs)
   

def test():
	"Test cases for the function in poker program"

	# some hands
	sf = "6C 7C 8C 9C TC".split() # straight flush
	fk = "9D 9H 9S 9C 7D".split() # four kind
	fh = "TD TC TH 7C 7D".split() # full house
	tp = "5S 5D 9S 9C 6S".split() # two pair
	s1 = "AS 2S 3S 4S 5C".split() # A-5 straight
	s2 = "2C 3C 4C 5S 6S".split() # 2-6 straight
	ah = "AS 2S 3S 4S 6C".split() # A high
	sh = "2S 3S 4S 6C 7D".split() # 7 high

	# tests for poker function
	assert poker([s1, s2, ah, sh]) == s2
	assert poker([sf, fk, fh]) == sf
	assert poker([fk, fh]) == fk
	assert poker([fh, fh]) == fh
	assert poker([sf]) == sf
	assert poker([sf for i in xrange(100)]) == sf


	# tests for hand_rank function
	assert hand_rank(sf) == (8, 10)
	assert hand_rank(fk) == (7, 9, 7)
	assert hand_rank(fh) == (6, 10, 7)
	return "tests pass" 

	

print test()
