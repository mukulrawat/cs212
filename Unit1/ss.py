# Example program to find out sum of squares to demonstrate functional style

def ss(nums):
    return sum([i**2 for i in nums])

print ss([1,2,3])
print ss([2,3,4])

